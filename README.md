## To-do items list application

### 1. Description

- This  an  __application__ that will manage to-do items with full CRUD functionality implemented.


### 2. Technical details

__Technologies used__

- main code is written in Java (minimum version: 8)
- it uses Spring MVC web framework and JSP Java technology(including JSTL tags and Tomcat) built on top of the Java Servlet specification.
- it was constructed with the help of Maven software project management and comprehension tool -> in the pom.xml file
I have added the following dependencies which were used within the whole project: Spring framework,
  logback, javax annotation, project lombok and javax servlet.


__Code structure__
- java code is organized in packages by its role, on layers:
  - config - used to initialize the web app(including bean methods, spring context)
  - controller - used to configure controllers for each of the existing pages
  - model - used to configure the object to-do item and the back-end implementation for CRUD functionality
  - service / util - used only for utility purposes

- in the resources folder there is only a logback.xml file, which was used within the whole project for logging purposes

- in the webapp directory there are the following jsp files, each one with a very well-defined scope
  - page to view all the to-do items - items_list.jsp
  - page to view single to-do items - view_item.jsp
  - a basic homepage with a hyperlink to the main table with all the to-do items - home.jsp
  - page for creating new to-do items - add_item.jsp
  
  

