<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="academy.learnprogramming.util.Mappings" %>
<html>
<head>
    <title>View item</title>
</head>
<body>
    <div align="center">

        <table border="1" cellpadding="5">

            <caption><h2>Todo Item</h2></caption>

            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Deadline</th>
                <th>Details</th>
            </tr>

            <tr>
                <td><c:out value="${todoItem.id}"/></td>
                <td><c:out value="${todoItem.title}"/></td>
                <td><c:out value="${todoItem.deadline}"/></td>
                <th><c:out value="${todoItem.details}"/></td>
            </tr>
        </table>

            <c:url var="tableUrl" value="${Mappings.ITEMS}"/>
            <h2><a href="${tableUrl}">Go back to full list of items!</a></h2>
    </div>

</body>
</html>